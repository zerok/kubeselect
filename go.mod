module gitlab.com/zerok/kubeselect

go 1.12

require (
	github.com/ktr0731/go-fuzzyfinder v0.1.2
	github.com/pkg/errors v0.8.1
	github.com/rs/zerolog v1.14.3
	github.com/spf13/afero v1.2.2
	github.com/spf13/cobra v0.0.4
	github.com/stretchr/testify v1.2.2
	gopkg.in/yaml.v2 v2.2.2
)
